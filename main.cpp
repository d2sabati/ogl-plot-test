#include <QApplication>
#include "ogl-plot-lib/openglwindow.h"
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    string dataDirectory = "/home/administrateur/Documents/ogl-plot-test/data/";

    vector<vector<double>> datas;
    vector<string> nameDataTest = {"dev-intersections.txt","dev-intersections-set1.txt","dev-intersections-set2.txt"};
    ifstream file;
    for(int i = 0; i < nameDataTest.size();i++){
        string name = dataDirectory+nameDataTest[i];
        file.open(name);
        if(file.is_open()){
           while(!file.eof()){
            stringstream convert;
            vector<double> data;

                string line;
                double x,y,z;
                getline(file,line);
                convert<<line;
                convert>>x;
                data.push_back(x);
               // getline(file,line,' ');
                convert<<line;
                convert>>y;
                data.push_back(y);
                //getline(file,line,' ');
                convert<<line;
                convert>>z;
                data.push_back(z);

            datas.push_back(data);
           }
            file.close();
        }else{
            cout<<"couldn´t open file"<<endl;
        }
    }

    OpenGLWindow *appWindow = new OpenGLWindow();
    appWindow->resize(1000,750);
    appWindow->show();

    //on clear l´affichage
    appWindow->clearGraph();
    //on set la taille dans opengGL des axes
    appWindow->setAxisSize(0.5f);
    int id = appWindow->newElement();
    int id1 = appWindow->newElement();
    int id2 = appWindow->newElement();

    for(int i = 0; i <datas.size();i++){
        cout<<datas[i].size()<<endl;
        //for (int j = 0; j < datas[i].size(); j+=3){
            if(datas[i][2] > 0.0 && datas[i][2]<0.1)
                appWindow->plotXYZ(id,datas[i][0],datas[i][1],datas[i][2]);
            else if(datas[i][2] >=0.1 && datas[i][2]<1)
                appWindow->plotXYZ(id1,datas[i][0],datas[i][1],datas[i][2]);
            else
                appWindow->plotXYZ(id2,datas[i][0],datas[i][1],datas[i][2]);
        //}
    }

    appWindow->setElementPointSize(0,5.0f);
    //on set la couleur des poinst de l´element 0
    appWindow->setElementPointColor(0,Qt::black);
    //o set la couleur des lignes de l´element 0
    appWindow->setElementLineColor(0,Qt::black);
    //on set le type d´affichage des points (points, lignes, surface, ligne avec points)
    appWindow->setElementType(0,LineType::Points);

    appWindow->setElementPointSize(1,5.0f);
    appWindow->setElementPointColor(1,Qt::red);
    appWindow->setElementType(1,LineType::Points);

    appWindow->setElementPointSize(2,5.0f);
    appWindow->setElementPointColor(2,Qt::green);
    appWindow->setElementType(2,LineType::Points);
    appWindow->setElementLabel(2,true);

    //appWindow->setDisplayGrid(false);
    appWindow->autoRange();
    //set la visualisation 2D/3D
    appWindow->set3D(true);
    /*
    //TORUS
    //on ajoute un element id1
    int id1 = appWindow->newElement();
    double x,y,z,ti,tj;

    for (int i = 0; i < 41; i++){
        ti = (i - 20.0)/20.0 * 3.15;

        for (int j = 0; j < 41 ; j++){
            tj = (j - 20.0)/20.0 * 3.15;

            x = (cos(tj) + 3.0) * cos(ti);
            y = sin(tj);
            z = (cos(tj) + 3.0) * sin(ti);

            //on ajoute des points a l´element id1
            appWindow->plotXYZ(id1,x,y,z);
        }
    }

    //on set une custom range
    //wid->setRange(-10, 10, -1, 1, -10, 10);
    //on set une range automatique
    appWindow->autoRange();
    //on set la couleur des poinst de l´element id1
    appWindow->setElementPointColor(id1,Qt::white);
    //o set la couleur des lignes de l´element id1
    appWindow->setElementLineColor(id1,Qt::red);
    //on set le type d´affichage des points (points, lignes, surface, ligne avec points)
    appWindow->setElementType(id1,LineType::Points);
    //on set la taille des points pour id1 (possible aussi avec les lignes)
    appWindow->setElementPointSize(id1,5.0f);
    //on set la valeur flat ou smooth de l´affichage pour l´element id1
    appWindow->setElementSurfaceFlat(id1,true);
    //si on set elementType a surface, on set si on fill ou pas
    //appWindow->setElementSurfaceFill(id1,false);
    */
    return a.exec();
}
